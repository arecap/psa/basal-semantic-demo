package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.arecap.spot.basalsemanticdemo.semanticmodel.jpa.BasalModel;
import org.springframework.aop.support.DelegatingIntroductionInterceptor;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public class BaseModelEntityIntroductionInterceptor extends DelegatingIntroductionInterceptor implements BasalModelEntity {


    @Override
    public Class<?> getEntityType() {
        return null;
    }

    @Override
    public Map<String, BasalModel> getBasalModel() {
        return null;
    }

    @Override
    public List<BasalModelEntity> getRoots() {
        return null;
    }

    @Override
    public List<Page<BasalModelEntity>> getRootPages() {
        return null;
    }

}
