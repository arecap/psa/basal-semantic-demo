package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.springframework.data.repository.config.RepositoryBeanDefinitionRegistrarSupport;
import org.springframework.data.repository.config.RepositoryConfigurationExtension;

import java.lang.annotation.Annotation;

public class SpotBasalModelRepositoryRegistrar extends RepositoryBeanDefinitionRegistrarSupport {

    @Override
    protected Class<? extends Annotation> getAnnotation() {
        return EnableSpotRepositories.class;
    }

    @Override
    protected RepositoryConfigurationExtension getExtension() {
        return new SpotBasalModelRepositoryConfigExtension();
    }

}
