package org.arecap.spot.basalsemanticdemo.semanticmodel.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PgBasalModelRepository extends PagingAndSortingRepository<BasalModel, Integer> {
}
