package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.arecap.spot.basalsemanticdemo.semanticmodel.jpa.BasalModel;
import org.springframework.data.repository.core.EntityInformation;

import java.util.Map;

public class BasalModelEntityInformation implements EntityInformation<Object, Integer> {


    private Class<?> entityType;

    public BasalModelEntityInformation(Class<?> entityType) {
        this.entityType = entityType;
    }

    @Override
    public boolean isNew(Object entity) {
        return !BasalModelEntity.class.isAssignableFrom(entity.getClass());
    }

    @Override
    public Integer getId(Object entity) {
        return isNew(entity) ? getSpotGroup(((BasalModelEntity)entity).getBasalModel()) : null;
    }

    private Integer getSpotGroup(Map<String, BasalModel> basalModelContext) {
        return basalModelContext.values().stream().findAny().get().getSpotGroup();
    }

    @Override
    public Class getIdType() {
        return Integer.class;
    }

    @Override
    public Class getJavaType() {
        return entityType;
    }
}
