package org.arecap.spot.basalsemanticdemo.semanticmodel;


import org.arecap.spot.basalsemanticdemo.semanticmodel.jpa.BasalModel;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface BasalModelEntity {

    Class<?> getEntityType();

    Map<String, BasalModel> getBasalModel();

    List<BasalModelEntity> getRoots();

    List<Page<BasalModelEntity>> getRootPages();
}
