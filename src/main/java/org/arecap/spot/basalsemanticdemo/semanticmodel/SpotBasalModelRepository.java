package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface SpotBasalModelRepository<T> extends PagingAndSortingRepository<T, Integer> {
}
