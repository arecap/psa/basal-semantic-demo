/*
 * Copyright 2020 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.arecap.spot.basalsemanticdemo.semanticmodel.jpa;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

/**
 * The persistence semantic logic model of spot
 *
 * @author Octavian Stirbei
 * @since 1.0.0
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "basal_model")
public class BasalModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_generator")
    @SequenceGenerator( name = "id_generator", allocationSize = 1, sequenceName = "id_generator_seq")
    @Getter
    @Setter
    private Integer id;

    @Column(columnDefinition = "text", length = 500)
    @Getter
    @Setter
    private String tag;

    @Column
    @Getter
    @Setter
    private String model;

    @Column
    @Getter
    @Setter
    private String modelRoot;

    @Column
    @Getter
    @Setter
    private String spot;

    @Column
    @Getter
    @Setter
    private String spotRoot;

    @Column
    @Getter
    @Setter
    private Integer spotGroup;

    @Column(columnDefinition = "text")
    @Getter
    @Setter
    private String spotValue;


}
