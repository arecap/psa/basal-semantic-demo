package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.springframework.data.repository.core.EntityInformation;
import org.springframework.data.repository.core.RepositoryInformation;
import org.springframework.data.repository.core.RepositoryMetadata;
import org.springframework.data.repository.core.support.RepositoryFactorySupport;
import org.springframework.data.repository.query.QueryLookupStrategy;
import org.springframework.data.repository.query.QueryMethodEvaluationContextProvider;

import java.util.Optional;

public class SpotBasalModelRepositoryFactory extends RepositoryFactorySupport {

    @Override
    public  EntityInformation<Object, Integer> getEntityInformation(Class aClass) {
        return new BasalModelEntityInformation(aClass);
    }

    @Override
    protected Object getTargetRepository(RepositoryInformation repositoryInformation) {
        return getTargetRepositoryViaReflection(repositoryInformation.getRepositoryBaseClass());
    }

    @Override
    protected Class<?> getRepositoryBaseClass(RepositoryMetadata repositoryMetadata) {
        return DefaultSpotBasalModelRepository.class;
    }

    @Override
    protected Optional<QueryLookupStrategy> getQueryLookupStrategy(QueryLookupStrategy.Key key, QueryMethodEvaluationContextProvider evaluationContextProvider) {
        return Optional.of(new SpotBasalModelQueryLookupStrategy());
    }

}
