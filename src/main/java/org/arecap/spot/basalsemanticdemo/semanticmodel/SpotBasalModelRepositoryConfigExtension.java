package org.arecap.spot.basalsemanticdemo.semanticmodel;

import org.springframework.data.repository.config.RepositoryConfigurationExtensionSupport;

public class SpotBasalModelRepositoryConfigExtension extends RepositoryConfigurationExtensionSupport {

    @Override
    protected String getModulePrefix() {
        return "spot-basal-repository";
    }

    @Override
    public String getRepositoryFactoryBeanClassName() {
        return SpotBasalModelRepositoryFactoryBean.class.getName();
    }

}
