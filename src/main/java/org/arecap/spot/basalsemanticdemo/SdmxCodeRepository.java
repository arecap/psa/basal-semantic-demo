package org.arecap.spot.basalsemanticdemo;

import org.springframework.data.repository.PagingAndSortingRepository;

public interface SdmxCodeRepository extends PagingAndSortingRepository<SdmxCode, String> {
}
