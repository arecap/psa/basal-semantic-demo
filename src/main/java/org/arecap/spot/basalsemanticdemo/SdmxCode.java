package org.arecap.spot.basalsemanticdemo;


import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

public class SdmxCode {

    @Getter @Setter @NonNull
    private String id;

    @Getter @Setter @NonNull
    private String name;

    @Getter @Setter
    private String description;

    @Getter @Setter
    private String annotation;

    @Getter @Setter
    private String uri;

    @Getter @Setter
    private SdmxCodeList codeList;


}
