package org.arecap.spot.basalsemanticdemo;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;


@Route("sdmx-code-list")
public class SdmxCodeListRoute extends VerticalLayout {

    private final static Logger logger = LoggerFactory.getLogger(SdmxCodeListRoute.class);

    private TextField searchTextField = new TextField();

    private Button searchButton = new Button(VaadinIcon.SEARCH.create());

    private Grid<SdmxCodeList> sdmxCodeListGrid = new Grid<>(SdmxCodeList.class);

    @PostConstruct
    void postProcessingConstructor() {
        add(new HorizontalLayout(searchTextField, searchButton), sdmxCodeListGrid);
    }
}
