package org.arecap.spot.basalsemanticdemo;

import org.arecap.spot.basalsemanticdemo.semanticmodel.EnableSpotRepositories;
import org.arecap.spot.basalsemanticdemo.semanticmodel.jpa.PgBasalModelRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableSpotRepositories(excludeFilters = {
		@ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = {PgBasalModelRepository.class})
})
@EnableJpaRepositories(basePackages = {"org.arecap.spot.basalsemanticdemo.semanticmodel.jpa"})
public class BasalSemanticDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(BasalSemanticDemoApplication.class, args);
	}

}
