package org.arecap.spot.basalsemanticdemo;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.Date;

public class SdmxCodeList {

    @Getter
    @Setter
    @NonNull
    private String id;

    @Getter @Setter @NonNull
    private String version;

    @Getter @Setter @NonNull
    private String number;

    @Getter @Setter @NonNull
    private String agency;

    @Getter @Setter @NonNull
    private String name;

    @Getter @Setter
    private String description;

    @Getter @Setter
    private String annotation;

    @Getter @Setter
    private String uri;

    @Getter @Setter
    private Date validTo;

    @Getter @Setter
    private Date validFrom;

    @Getter @Setter
    private Boolean open;

    @Getter @Setter
    private Page<SdmxCode> codes;


    @Getter @Setter
    private Page<SdmxCodeList> hierarchies;

}
