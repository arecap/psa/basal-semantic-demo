---sequence
CREATE SEQUENCE IF NOT EXISTS id_generator_seq;

-- envers audit config
CREATE TABLE IF NOT EXISTS envers_rev_entity
(
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    username character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT envers_rev_entity_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--ff4j entities

CREATE TABLE IF NOT EXISTS ff4j_audit
(
    evt_uuid character varying(40) COLLATE pg_catalog."default" NOT NULL,
    evt_time timestamp without time zone NOT NULL,
    evt_type character varying(30) COLLATE pg_catalog."default" NOT NULL,
    evt_name character varying(30) COLLATE pg_catalog."default" NOT NULL,
    evt_action character varying(30) COLLATE pg_catalog."default" NOT NULL,
    evt_hostname character varying(100) COLLATE pg_catalog."default" NOT NULL,
    evt_source character varying(30) COLLATE pg_catalog."default" NOT NULL,
    evt_duration integer,
    evt_user character varying(30) COLLATE pg_catalog."default",
    evt_value character varying(100) COLLATE pg_catalog."default",
    evt_keys character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT ff4j_audit_pkey PRIMARY KEY (evt_uuid, evt_time)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS ff4j_features
(
    feat_uid character varying(100) COLLATE pg_catalog."default" NOT NULL,
    enable integer NOT NULL,
    description character varying(1000) COLLATE pg_catalog."default",
    strategy character varying(1000) COLLATE pg_catalog."default",
    expression character varying(255) COLLATE pg_catalog."default",
    groupname character varying(100) COLLATE pg_catalog."default",
    CONSTRAINT ff4j_features_pkey PRIMARY KEY (feat_uid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS ff4j_properties
(
    property_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    clazz character varying(255) COLLATE pg_catalog."default" NOT NULL,
    currentvalue character varying(255) COLLATE pg_catalog."default",
    fixedvalues character varying(1000) COLLATE pg_catalog."default",
    description character varying(1000) COLLATE pg_catalog."default",
    CONSTRAINT ff4j_properties_pkey PRIMARY KEY (property_id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS ff4j_custom_properties
(
    property_id character varying(100) COLLATE pg_catalog."default" NOT NULL,
    clazz character varying(255) COLLATE pg_catalog."default" NOT NULL,
    currentvalue character varying(255) COLLATE pg_catalog."default",
    fixedvalues character varying(1000) COLLATE pg_catalog."default",
    description character varying(1000) COLLATE pg_catalog."default",
    feat_uid character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ff4j_custom_properties_pkey PRIMARY KEY (property_id, feat_uid),
    CONSTRAINT ff4j_custom_properties_feat_uid_fkey FOREIGN KEY (feat_uid)
        REFERENCES ff4j_features (feat_uid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;



CREATE TABLE IF NOT EXISTS ff4j_roles
(
    feat_uid character varying(100) COLLATE pg_catalog."default" NOT NULL,
    role_name character varying(100) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT ff4j_roles_pkey PRIMARY KEY (feat_uid, role_name),
    CONSTRAINT ff4j_roles_feat_uid_fkey FOREIGN KEY (feat_uid)
        REFERENCES ff4j_features (feat_uid) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

CREATE TABLE IF NOT EXISTS flyway_schema_history
(
    installed_rank integer NOT NULL,
    version character varying(50) COLLATE pg_catalog."default",
    description character varying(200) COLLATE pg_catalog."default" NOT NULL,
    type character varying(20) COLLATE pg_catalog."default" NOT NULL,
    script character varying(1000) COLLATE pg_catalog."default" NOT NULL,
    checksum integer,
    installed_by character varying(100) COLLATE pg_catalog."default" NOT NULL,
    installed_on timestamp without time zone NOT NULL DEFAULT now(),
    execution_time integer NOT NULL,
    success boolean NOT NULL,
    CONSTRAINT flyway_schema_history_pk PRIMARY KEY (installed_rank)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

--attachments entities

CREATE TABLE IF NOT EXISTS basal_model
(
    id integer NOT NULL,
    tag character varying(500) COLLATE pg_catalog."default",
    model character varying(255) COLLATE pg_catalog."default",
    model_root character varying(255) COLLATE pg_catalog."default",
    spot character varying(255) COLLATE pg_catalog."default",
    spot_root character varying(255) COLLATE pg_catalog."default",
    spot_group integer NOT NULL,
    spot_value character varying COLLATE pg_catalog."default",
    CONSTRAINT basal_model_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


